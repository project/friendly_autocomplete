# Friendly Autocomplete

Provides an improved autocomplete form API element. This module is intended for
use by developers who are creating custom forms.
